<?php

require_once 'Dio.php';

Dio::init();
Orror::init(E_ALL);

$doc = new Dio_Archive(Dio_Document::TYPE_TEXT);

// metadata
$m = $doc->metas;
$m->setCreator("Dumb creator");
$m->setTitle("Dumb title");
$m->setSubject("Dumb subject");
$m->setDescription("Dumb description");
$m->addKeywords("dio","test","odt","opendocument","php");

// polices
$ff = $doc->fonts;
$dv = $ff->addFontFace("DejaVu Sans");
$dvs = $ff->addFontFace("DejaVu Serif", "Book");

// styles
$ss = $doc->styles;
$s = $ss->addStyle_Default(Dio_Style::FAMILY_PARAGRAPH);
$p = $s->addPropertiesText();
$p->setFont($dv);
$p->setSize("10pt");

$std = $ss->addStyle("Standard", Dio_Style::FAMILY_PARAGRAPH, null, 'text');

$tb = $ss->addStyle("Text Body", Dio_Style::FAMILY_PARAGRAPH, $std, 'text');
$p = $tb->addPropertiesText();
$p->setFont($dvs);
$p->setSize("10pt");

$h = $ss->addStyle("Heading", Dio_Style::FAMILY_PARAGRAPH, $std, 'text', $tb);
$p = $s->addPropertiesText();
$p->setSize("14pt");
$p = $h->addPropertiesParagraph();
$p->setMarginTop("0.423cm");
$p->setMarginBottom("0.212cm");

$h1 = $ss->addStyle("Heading 1", Dio_Style::FAMILY_PARAGRAPH, $h, 'text', $tb);
$p = $h1->addPropertiesText();
$p->setSize("115%");
$p->setWeight("bold");

$h2 = $ss->addStyle("Heading_2", Dio_Style::FAMILY_PARAGRAPH, $h, 'text');
$p = $h2->addPropertiesText();
$p->setSize("14pt");
$p->setWeight("bold");
$p->setFontStyle(Dio_Style_Properties_Text::FONT_STYLE_ITALIC);

// content
$t = $doc->content;
$h = $t->openSection();
$a = new Dio_Text_A("Test link", "http://www.google.com");
$h->appendChild($a);
$t->openSection("Test subheading");
$t->addP("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed velit turpis, laoreet ut, egestas nec, fringilla at, pede. Maecenas justo ipsum, aliquet at, ornare non, interdum quis, diam. Nunc ultrices dolor non elit pulvinar fringilla. Duis non eros. Morbi a massa eu lacus pharetra placerat. Nullam id leo. Vivamus convallis bibendum quam. Nunc dolor. In sit amet turpis quis turpis rhoncus faucibus. Nunc non eros eget sapien faucibus placerat. Maecenas tristique nisi ut quam. Cras scelerisque tortor eget lectus.");
$t->closeSection();
$t->openSection("Test subheading 2");
$p = $t->addP("Etiam purus leo, cursus fringilla, aliquam sit amet, vestibulum sit amet, dolor. Nulla facilisi. Aenean leo nulla, pellentesque quis, elementum id, posuere vel, turpis. Integer velit purus, egestas ac, auctor at, commodo iaculis, diam. Sed aliquet, dolor sit amet adipiscing porta, quam ligula congue est, quis consequat leo sem nec elit. Praesent justo nulla, pulvinar ac, hendrerit sed, laoreet eu, massa. Morbi pharetra turpis quis lacus. Proin porttitor lectus mattis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut pulvinar augue et dolor. Suspendisse sit amet elit id turpis hendrerit aliquam. Duis vestibulum, ipsum vitae vestibulum faucibus, tortor justo ullamcorper ipsum, in luctus turpis arcu et lectus. In risus nisi, pharetra eget, varius et, facilisis in, mauris. Nam molestie elementum nulla. Curabitur euismod iaculis nunc. Proin posuere. Fusce varius justo vel lectus. Fusce sit amet nibh et nisi aliquam bibendum. Quisque vitae nibh. ");

// output
if (!headers_sent()) {
	if (1) {
		header("Content-type: ".$doc->mimetype);
		header("Content-Disposition: attachment; filename=dio-text.fodt");
	}
	else {
		header("Content-type: text/plain");
	}
	echo $doc->render();
 }

$doc = null;