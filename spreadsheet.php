<?php

require_once 'Dio.php';

Dio::init();

if (1) {
	$doc = new Dio_Archive(Dio_Document::TYPE_SPREADSHEET);
	$suffix = 'ods';
 }
 else {
	$doc = new Dio_Flat(Dio_Document::TYPE_SPREADSHEET);
	$suffix = 'fods';
 }

// metadata
$m = $doc->metas;
$m->setCreator("Dumb creator");
$m->setTitle("Dumb title");
$m->setSubject("Dumb subject");
$m->setDescription("Dumb description");
$m->addKeywords("dio","test","ods","opendocument","php");


// polices
$ff = $doc->fonts;
$dv = $ff->addFontFace("DejaVu Sans");
$dvs = $ff->addFontFace("DejaVu Serif", "Book");

// styles
$ss = $doc->styles;
$s = $ss->addStyle_Default(Dio_Style::FAMILY_TABLE_CELL);
$p = $s->addPropertiesTableCell();
$p->setDecimalPlaces(2);
$p = $s->addPropertiesParagraph();
$p->setTabStopDistance('1.15cm');
$p = $s->addPropertiesText();
$p->setFont($dv);
$p->setSize("10pt");

$std = $ss->addStyle("Default", Dio_Style::FAMILY_TABLE_CELL);

$tb = $ss->addStyle("Text Body", Dio_Style::FAMILY_TABLE_CELL, $std);
$p = $tb->addPropertiesText();
$p->setFont($dvs);
$p->setSize("10pt");

$bold = $ss->addStyle("Bold", Dio_Style::FAMILY_TABLE_CELL, $std);
$p = $bold->addPropertiesText();
$p->setWeight(Dio_Style_Properties_Text::WEIGHT_BOLD);

$h = $ss->addStyle("Heading", Dio_Style::FAMILY_TABLE_CELL, $std, $tb);
$p = $h->addPropertiesText();
$p->setSize("14pt");
$p = $h->addPropertiesParagraph();
$p->setMarginTop("0.423cm");
$p->setMarginBottom("0.212cm");

$h1 = $ss->addStyle("Heading 1", Dio_Style::FAMILY_TABLE_CELL, $h, $tb);
$p = $h1->addPropertiesText();
$p->setSize("14pt");
$p->setWeight("bold");

$h2 = $ss->addStyle("Heading 2", Dio_Style::FAMILY_TABLE_CELL, $h);
$p = $h2->addPropertiesText();
$p->setSize("13pt");
$p->setFontStyle(Dio_Style_Properties_Text::FONT_STYLE_ITALIC);
$p->setWeight("bold");

$h3 = $ss->addStyle("Heading 3", Dio_Style::FAMILY_TABLE_CELL, $h);
$p = $h3->addPropertiesText();
$p->setSize("11pt");
$p->setFontStyle(Dio_Style_Properties_Text::FONT_STYLE_ITALIC);
$p->setWeight("bold");
$p->setMarginTop("0.125cm");
$p->setMarginBottom("0.06125cm");


$y = 0;
// content
$s = $doc->content;
$t = $s->addTable('Test', array('Column 0', 'Column 1', 'Column 2'));
$c = $t->put(0, $y++, new Dio_Text_P('Ab uno enim primo'));
$c->setStyle($h1);
$y++;
$c = $t->put(array(0,4), $y++, 'Lorem ipsum dolor sit amet.');
$c->setStyle($h2);
$y++;
$t->put(0, $y++, 'Consectetuer adipiscing elit.');
$t->put(0, $y++, 'Maecenas mi est, mattis sit amet.');
$t->put(0, $y++, 'Sodales facilisis.');
$t->put(0, $y++, new Dio_Text_A("Ut sed enim in lectus porttitor ullamcorper.", "http://www.google.com/"));

$y++;
$t->put(array(1,2), $y++, new Dio_Text_P('Data type'))->setStyle($h3);
$y++;
$t->put(1, $y, 'Type')->setStyle($bold);
$t->put(2, $y, 'Value')->setStyle($bold);
$y++;
$data = array('string', 4.5, 3, true);
foreach($data as $d) {
	$t->put(1, $y, gettype($d));
	$t->put(2, $y, $d);
	$y++;
}

// output
if (!headers_sent()) {
	if (1) {
		header("Content-type: ".$doc->mimetype);
		header("Content-Disposition: attachment; filename=dio-spreadsheet.".$suffix);
	}
	else {
		header("Content-type: text/xml");
	}
	echo $doc->render();
 }

$doc = null;
