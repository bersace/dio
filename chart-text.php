<?php

require_once 'Dio.php';

Dio::init();
Orror::init(E_ALL);

if (0) {
	$doc = new Dio_Archive(Dio_Document::TYPE_TEXT);
	$suffix = 'odt';
 }
 else {
	$doc = new Dio_Flat(Dio_Document::TYPE_TEXT);
	$suffix = 'fodt';
 }

// metadata
$m = $doc->metas;
$m->setCreator("Dumb creator");
$m->setTitle("Dumb title");
$m->setSubject("Dumb subject");
$m->setDescription("Dumb description");
$m->addKeywords("dio","test","ods","opendocument","php","chart");


// polices
$ff = $doc->fonts;
$dv = $ff->addFontFace("DejaVu Sans");
$dvs = $ff->addFontFace("DejaVu Serif", "Book");

// styles
$ss = $doc->styles;
$s = $ss->addStyle_Default(Dio_Style::FAMILY_PARAGRAPH);
$p = $s->addPropertiesText();
$p->setFont($dv);
$p->setSize("10pt");

$std = $ss->addStyle("Standard", Dio_Style::FAMILY_PARAGRAPH, null, 'text');

$tb = $ss->addStyle("Text Body", Dio_Style::FAMILY_PARAGRAPH, $std, 'text');
$p = $tb->addPropertiesText();
$p->setFont($dvs);
$p->setSize("10pt");

$h = $ss->addStyle("Heading", Dio_Style::FAMILY_PARAGRAPH, $std, 'text', $tb);
$p = $s->addPropertiesText();
$p->setSize("14pt");
$p = $h->addPropertiesParagraph();
$p->setMarginTop("0.423cm");
$p->setMarginBottom("0.212cm");

$h1 = $ss->addStyle("Heading 1", Dio_Style::FAMILY_PARAGRAPH, $h, 'text', $tb);
$p = $h1->addPropertiesText();
$p->setSize("115%");
$p->setWeight("bold");

$h2 = $ss->addStyle("Heading_2", Dio_Style::FAMILY_PARAGRAPH, $h, 'text');
$p = $h2->addPropertiesText();
$p->setSize("14pt");
$p->setWeight("bold");
$p->setFontStyle(Dio_Style_Properties_Text::FONT_STYLE_ITALIC);

// content
$s = $doc->content;
$p = $s->addText_P();
// Diagram
$f = $p->addDraw_Frame("0cm", "0cm", "10cm", "5cm");
$o = $f->addDraw_Object();
$c = $o->embedOffice_Document(Dio_Document::TYPE_CHART, true);
$c = $c->content->addChart(Dio_Chart::TYPE_BAR, "10cm", "5cm");
$c->addLegend(Dio_Chart_Legend::POSITION_END);

$t = $c->addTable('Colonne A', 'Colonne B');
$t->name = 'local-table';
$size = 8;
for ($i = 0; $i < $size; $i++) {
	$t->put(0, $i, intval($i));
	$t->put(1, $i, intval(rand(0, 20)));
 }

// Ranges for diagram
$start0 = $t->get(0, 0);
$end0 = $t->get(0, $size-1);
$r0 = new Dio_Table_CellRangeAddress($start0->getAddress(), $end0->getAddress());

$start1 = $t->get(1, 0);
$end1 = $t->get(1, $size-1);
$r1 = new Dio_Table_CellRangeAddress($start1->getAddress(), $end1->getAddress());
$r = new Dio_Table_CellRangeAddress($start0->getAddress(), $end1->getAddress());

$p = $c->addPlotArea("0.5cm", "0.5cm", "7cm", "4.5cm", Dio_Chart_PlotArea::LABELS_COLUMN);
$x = $p->addAxis(Dio_Chart_Axis::DIMENSION_X, 'primary-x');
$x->addCategories($r0);
$y = $p->addAxis(Dio_Chart_Axis::DIMENSION_Y, 'primary-y');
$y->addGrid();
$s = $p->addSeries($r1);
$s->addDataPoint($size);
$p->addWall();
$p->addFloor();

// output
if (!headers_sent()) {
	if (1) {
		header("Content-type: ".$doc->mimetype);
		header("Content-Disposition: attachment; filename=dio-chart.".$suffix);
	}
	else {
		header("Content-type: text/xml");
	}
	echo $doc->render();
 }

$doc = null;