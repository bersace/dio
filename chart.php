<?php

require_once 'Dio.php';

Dio::init();
Orror::init(E_ALL);

if (1) {
	$doc = new Dio_Archive(Dio_Document::TYPE_SPREADSHEET);
	$suffix = 'ods';
 }
 else {
	$doc = new Dio_Flat(Dio_Document::TYPE_SPREADSHEET);
	$suffix = 'fods';
 }

// metadata
$m = $doc->metas;
$m->setCreator("Dumb creator");
$m->setTitle("Dumb title");
$m->setSubject("Dumb subject");
$m->setDescription("Dumb description");
$m->addKeywords("dio","test","ods","opendocument","php","chart");


// polices
$ff = $doc->fonts;
$dv = $ff->addFontFace("DejaVu Sans");
$dvs = $ff->addFontFace("DejaVu Serif", "Book");

// styles
$ss = $doc->styles;
$s = $ss->addStyle_Default(Dio_Style::FAMILY_TABLE_CELL);
$p = $s->addPropertiesTableCell();
$p->setDecimalPlaces(2);
$p = $s->addPropertiesParagraph();
$p->setTabStopDistance('1.15cm');
$p = $s->addPropertiesText();
$p->setFont($dv);
$p->setSize("10pt");

$std = $ss->addStyle("Default", Dio_Style::FAMILY_TABLE_CELL);

$tb = $ss->addStyle("Text Body", Dio_Style::FAMILY_TABLE_CELL, $std);
$p = $tb->addPropertiesText();
$p->setFont($dvs);
$p->setSize("10pt");


// content
$s = $doc->content;
$t = $s->addTable('Feuille1', array('Colonne A', 'Colonne B'));
$size = 8;
for ($i = 0; $i < $size; $i++) {
	$t->put(0, $i, intval($i));
	$t->put(1, $i, intval(rand(0, 20)));
 }

// Ranges for diagram
$start0 = $t->get(0, 1);
$end0 = $t->get(0, $size-1);
// X 
$r0 = new Dio_Table_CellRangeAddress($start0->getAddress(), $end0->getAddress());

$start1 = $t->get(1, 1);
$end1 = $t->get(1, $size-1);

// Y
$r1 = new Dio_Table_CellRangeAddress($start1->getAddress(), $end1->getAddress());
$r = new Dio_Table_CellRangeAddress($start0->getAddress(), $end1->getAddress());

// Diagram
$f = new Dio_Draw_Frame("0cm", "0cm", "10cm", "5cm");
$t->put(3, 0, $f);
$o = $f->addDraw_Object(array($r0, $r1));
$c = $o->embedOffice_Document(Dio_Document::TYPE_CHART, true);
$c = $c->content->addChart(Dio_Chart::TYPE_BAR, "10cm", "5cm");
$c->addLegend(Dio_Chart_Legend::POSITION_END);
$p = $c->addPlotArea("0.5cm", "0.5cm", "7cm", "4.5cm", $r, Dio_Chart_PlotArea::LABELS_COLUMN);
$x = $p->addAxis(Dio_Chart_Axis::DIMENSION_X, 'primary-x');
$x->addCategories($r0);
$y = $p->addAxis(Dio_Chart_Axis::DIMENSION_Y, 'primary-y');
$y->addGrid();
$s = $p->addSeries($r1);
$s->addDataPoint($size);
$p->addWall();
$p->addFloor();

// output
if (!headers_sent()) {
	if (1) {
		header("Content-type: ".$doc->mimetype);
		header("Content-Disposition: attachment; filename=dio-chart.".$suffix);
	}
	else {
		header("Content-type: text/xml");
	}
	echo $doc->render();
 }

$doc = null;